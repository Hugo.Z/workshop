/*
** EPITECH PROJECT, 2019
** workshop
** File description:
** Unit tests for `my_function`.
*/

#include <criterion/criterion.h>

#include "workshop.h"

Test(my_function, test_with_zero)
{
    cr_assert_eq(-1, my_function(0));
}
