# Maîtrisez l'intégration continue avec GitLab

## Introduction

Ce dépot contient une architecture classique pour un projet C.  
Vous y trouverez du code prêt à être compilé et executé ainsi que des fichiers qui vous serviront d'exemple.

### Première étape

Nous vous invitons à `forker` ce projet en cliquant sur le bouton prévu à cet effet en haut à gauche de cette page.  
La [documentation de GitLab](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) peut aussi vous aider.

### Deuxième étape

Tout d'abord, commencez par compiler le programme:

```bash
$ make
```

Prenez le temps d'éxecuter le programme et de comprendre son fonctionnement.  
Son code source a été commenté pour faciliter la compréhension.

### Troisième étape

Ce que nous vous demandons maintenant est de réaliser une pipeline pour tester ce projet.  
Pour ce faire, vous pouvez modifier le fichier `.gitlab-ci.yml` présent à la racine de ce dépot.


## Pour aller plus loin

Vous pouvez maintenant, en théorie, tester n'importe lequel de vos projets!
Lancez-vous!
