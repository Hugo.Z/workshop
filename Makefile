##
## EPITECH PROJECT, 2019
## workshop
## File description:
## The project build instructions.
##

CC = gcc
CFLAGS = -Wall -Wextra -Werror -O2 -Iinclude

NAME = workshop

SRCS = $(filter-out src/main.c, $(wildcard src/*.c))
OBJS = $(SRCS:.c=.o)

TEST_NAME = unit-tests

TEST_SRCS = $(wildcard test/*.c)
TEST_OBJS = $(TEST_SRCS:.c=.o)

all: $(NAME) $(TEST_NAME)

$(NAME): src/main.o $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)

test: $(TEST_NAME)

$(TEST_NAME): LDLIBS += -lcriterion
$(TEST_NAME): $(OBJS) $(TEST_OBJS)
	$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)

clean:
	$(RM) src/*.o src/*.gc*
	$(RM) test/*.o test/*.gc*

fclean: clean
	$(RM) $(NAME)
	$(RM) $(TEST_NAME)

re: fclean all

.PHONY: all clean fclean re test
